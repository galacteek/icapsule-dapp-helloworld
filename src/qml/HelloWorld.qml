import QtQuick 2.2

import GForce 1.0
import GForce.ipfs 1.0
import GForce.forms 1.0

GEmbeddedDapp {
  IpfsOperator {
    id: ipfsop
  }

  function inject(text) {
    let rid = ipfsop.addStr(text)
  }

  Rectangle {
    color: '#323232'
    anchors.fill: parent
    anchors.centerIn: parent

    Column {
      anchors.fill: parent
      spacing: 24

      ITextArea {
        anchors.horizontalCenter: parent.horizontalCenter
        readOnly: true
        font.pixelSize: 32
        width: parent.width * 0.7
        text: qsTr('Enter some text, hit enter and discover the corresponding CID !')
      }

      ITextField {
        id: content
        text: 'Hello world'
        anchors.horizontalCenter: parent.horizontalCenter

        onAccepted: {
          /* The user hit enter, inject the text in IPFS */
          inject(text)
        }
      }

      ITextArea {
        id: helloCid
        readOnly: true
        width: parent.width * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 24
      }

      IButton {
        text: qsTr('Copy the CID to the clipboard')
        width: parent.width * 0.5
        anchors.horizontalCenter: parent.horizontalCenter

        onClicked: {
          helloCid.select(0, helloCid.text.length)
          helloCid.copy()
        }
      }
    }
  }

  Connections {
    target: ipfsop

    function onOpDone(opid, result) {
      helloCid.text = result.Hash
    }
  }

  Component.onCompleted: {
    /* Inject the textfield's initial content (Hello world) */
    inject(content.text)
  }
}
